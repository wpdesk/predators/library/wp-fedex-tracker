## [1.2.0] - 2024-01-13
### Added
- api type

## [1.1.0] - 2022-10-18
### Removed
- opt in/opt out
- notice screens

## [1.0.3] - 2022-08-16
### Changed
- required library

## [1.0.2] - 2022-01-21
### Added
- custom services

## [1.0.0] - 2022-01-20
### Added
- initial version
