[![pipeline status](https://gitlab.com/wpdesk/predators/library/wp-fedex-tracker/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/predators/library/wp-fedex-tracker/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/predators/library/wp-fedex-tracker/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/predators/library/wp-fedex-tracker/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/predators/library/wp-fedex-tracker/v/stable)](https://packagist.org/packages/wpdesk/predators/library/wp-fedex-tracker) 
[![Total Downloads](https://poser.pugx.org/wpdesk/predators/library/wp-fedex-tracker/downloads)](https://packagist.org/packages/wpdesk/predators/library/wp-fedex-tracker) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/predators/library/wp-fedex-tracker/v/unstable)](https://packagist.org/packages/wpdesk/predators/library/wp-fedex-tracker) 
[![License](https://poser.pugx.org/wpdesk/predators/library/wp-fedex-tracker/license)](https://packagist.org/packages/wpdesk/predators/library/wp-fedex-tracker) 

FedEx Tracker
=============
